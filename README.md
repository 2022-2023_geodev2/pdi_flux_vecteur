# Rasterisation d'un Flux Vecteur

## Qu'est-ce que c'est ?

Rasterisation d'un Flux Vecteur est une interface Web permettant à l'utilisateur de créer une carte raster à partir d'un flux de tuiles vecteur et d'un fichier de style chargé, afin de l'exporter au format PNG ou PDF pour impression. La rasterisation se fait ainsi du côté client.

## Tutoriel d'installation

* Cloner ou télécharger le repository git du projet en local
* Utiliser un serveur local, par exemple MAMP. Configurer la racine du serveur en y mettant le chemin du dossier du projet.
* Après avoir lancé le serveur, ouvrir dans un navigateur l'URL : http://localhost

## Licences

Le produit utilise des bibliothèques sous licence BSD ou MIT regroupées dans le dossier [js/modules_externes](js/modules_externes). Ce dossier comprend les licences associées. 

## Auteurs

Edmond Saint Denis, Solène Thévenet, Emilie Vautier et Claire Guerrini, *Etudiants en 2e Année du cycle Ingénieur à l'ENSG-Géomatique*.

## Commanditaire

Lucille Ricaud, *Responsable du développement en cartographie écran à l’IGN*. 

## Logos
<img src="/img/IGN_logo.svg" alt= "IGN_logo" width="100px" height="100px">
<img src="/img/ENSG_logo.png" alt= "ENSG_logo" width="100px" height="100px">

