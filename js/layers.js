/**
 * Couches affichées sur les cartes de l'application principale.
 */

/*-------------------------- Définition des couches de la carte principale --------------------------*/

/** Couche IGN de la carte principale */
export var planignLayer = new ol.layer.VectorTile({
    title: "Plan IGN",
    description: "Plan IGN Vecteur tuilé",
    mapboxStyleUrl: "https://wxs.ign.fr/essentiels/static/vectorTiles/styles/PLAN.IGN/standard.json",
    mapboxStyleSource: "plan_ign",
    visible: true,
    source: new ol.source.VectorTile({
        format: new ol.format.MVT(),
        url: "https://wxs.ign.fr/essentiels/geoportail/tms/1.0.0/PLAN.IGN/{z}/{x}/{y}.pbf"
    }),
    declutter: true,
    attributions: '<a href="https://geoservices.ign.fr/documentation/geoservices/vecteur-tuile.html">&copy; IGN</a>',
    });

/** Couche BD Topo de la carte principale */
export var bdtopo = new ol.layer.VectorTile({
    title: "BDTOPO",
    description: "BDTOPO Vecteur tuilé",
    visible: false,
    mapboxStyleUrl: "https://wxs.ign.fr/static/vectorTiles/styles/BDTOPO/classique.json",
    mapboxStyleSource: "bdtopo",
    source: new ol.source.VectorTile({
        format: new ol.format.MVT(),
        url: "https://wxs.ign.fr/topographie/geoportail/tms/1.0.0/BDTOPO/{z}/{x}/{y}.pbf"
    }),
    declutter: true,
    attributions: '<a href="https://geoservices.ign.fr/blog/2020/10/26/Grand_angle_diffusion_BDTOPO.html">BDTOPO</a>',
});

/** Couche OSM (raster) de la carte principale */
export var osmLyr = new ol.layer.Tile({
    source: new ol.source.OSM(),
    title : "OSM",
    description: "Couche OpenStreet Map",
    visible : false
});


/*-------------------------- Définition des couches de la loupe --------------------------*/

export var planignLayerloupe = new ol.layer.VectorTile({
    title: "Plan IGN",
    description: "Plan IGN Vecteur tuilé",
    mapboxStyleUrl: "https://wxs.ign.fr/essentiels/static/vectorTiles/styles/PLAN.IGN/standard.json",
    mapboxStyleSource: "plan_ign",
    visible: true,
    source: new ol.source.VectorTile({
        format: new ol.format.MVT(),
        url: "https://wxs.ign.fr/essentiels/geoportail/tms/1.0.0/PLAN.IGN/{z}/{x}/{y}.pbf"
    }),
    declutter: false,
    attributions: '<a href="https://geoservices.ign.fr/documentation/geoservices/vecteur-tuile.html">&copy; IGN</a>',
});


export var bdtopoloupe = new ol.layer.VectorTile({
    title: "BDTOPO",
    description: "BDTOPO Vecteur tuilé",
    visible: false,
    mapboxStyleUrl: "https://wxs.ign.fr/static/vectorTiles/styles/BDTOPO/classique.json",
    mapboxStyleSource: "bdtopo",
    source: new ol.source.VectorTile({
        format: new ol.format.MVT(),
        url: "https://wxs.ign.fr/topographie/geoportail/tms/1.0.0/BDTOPO/{z}/{x}/{y}.pbf"
    }),
    declutter: false,
    attributions: '<a href="https://geoservices.ign.fr/blog/2020/10/26/Grand_angle_diffusion_BDTOPO.html">BDTOPO</a>',
    });


export var osmLyrloupe = new ol.layer.Tile({
    source: new ol.source.OSM(),
    title : "OSM",
    description: "Couche OpenStreet Map",
    visible :false
});


/*-------------------------- Définition des couches des cartes secondaires --------------------------*/
export var planignLayer1 = new ol.layer.VectorTile({
    title: "Plan IGN",
    description: "Plan IGN Vecteur tuilé",
    mapboxStyleUrl: "https://wxs.ign.fr/essentiels/static/vectorTiles/styles/PLAN.IGN/standard.json",
    mapboxStyleSource: "plan_ign",
    visible: true,
    source: new ol.source.VectorTile({
        format: new ol.format.MVT(),
        url: "https://wxs.ign.fr/essentiels/geoportail/tms/1.0.0/PLAN.IGN/{z}/{x}/{y}.pbf"
    }),
    declutter: true,
    attributions: '<a href="https://geoservices.ign.fr/documentation/geoservices/vecteur-tuile.html">&copy; IGN</a>',
    });


export var bdtopo1 = new ol.layer.VectorTile({
    title: "BDTOPO",
    description: "BDTOPO Vecteur tuilé",
    visible: false,
    mapboxStyleUrl: "https://wxs.ign.fr/essentiels/static/vectorTiles/styles/BDTOPO/classique.json",
    mapboxStyleSource: "bdtopo",
    source: new ol.source.VectorTile({
        format: new ol.format.MVT(),
        url: "https://wxs.ign.fr/topographie/geoportail/tms/1.0.0/BDTOPO/{z}/{x}/{y}.pbf"
    }),
    declutter: true,
    attributions: '<a href="https://geoservices.ign.fr/blog/2020/10/26/Grand_angle_diffusion_BDTOPO.html">BDTOPO</a>',
});


export var osmLyr1 = new ol.layer.Tile({
    source: new ol.source.OSM(),
    title : "OSM",
    description: "Couche OpenStreet Map",
    visible : false

});


export var planignLayer2 = new ol.layer.VectorTile({
    title: "Plan IGN",
    description: "Plan IGN Vecteur tuilé",
    mapboxStyleUrl: "https://wxs.ign.fr/essentiels/static/vectorTiles/styles/PLAN.IGN/standard.json",
    mapboxStyleSource: "plan_ign",
    visible: true,
    source: new ol.source.VectorTile({
        format: new ol.format.MVT(),
        url: "https://wxs.ign.fr/essentiels/geoportail/tms/1.0.0/PLAN.IGN/{z}/{x}/{y}.pbf"
    }),
    declutter: true,
    attributions: '<a href="https://geoservices.ign.fr/documentation/geoservices/vecteur-tuile.html">&copy; IGN</a>',
    });


export var bdtopo2 = new ol.layer.VectorTile({
    title: "BDTOPO",
    description: "BDTOPO Vecteur tuilé",
    visible: false,
    mapboxStyleUrl: "https://wxs.ign.fr/static/vectorTiles/styles/BDTOPO/classique.json",
    mapboxStyleSource: "bdtopo",
    source: new ol.source.VectorTile({
        format: new ol.format.MVT(),
        url: "https://wxs.ign.fr/topographie/geoportail/tms/1.0.0/BDTOPO/{z}/{x}/{y}.pbf"
    }),
    declutter: true,
    attributions: '<a href="https://geoservices.ign.fr/blog/2020/10/26/Grand_angle_diffusion_BDTOPO.html">BDTOPO</a>',
});


export var osmLyr2 = new ol.layer.Tile({
    source: new ol.source.OSM(),
    title : "OSM",
    description: "Couche OpenStreet Map",
    visible : false
});


export var planignLayer3 = new ol.layer.VectorTile({
    title: "Plan IGN",
    description: "Plan IGN Vecteur tuilé",
    mapboxStyleUrl: "https://wxs.ign.fr/essentiels/static/vectorTiles/styles/PLAN.IGN/standard.json",
    mapboxStyleSource: "plan_ign",
    visible: true,
    source: new ol.source.VectorTile({
        format: new ol.format.MVT(),
        url: "https://wxs.ign.fr/essentiels/geoportail/tms/1.0.0/PLAN.IGN/{z}/{x}/{y}.pbf"
    }),
    declutter: true,
    attributions: '<a href="https://geoservices.ign.fr/documentation/geoservices/vecteur-tuile.html">&copy; IGN</a>',
    });


export var bdtopo3 = new ol.layer.VectorTile({
    title: "BDTOPO",
    description: "BDTOPO Vecteur tuilé",
    visible: false,
    mapboxStyleUrl: "https://wxs.ign.fr/static/vectorTiles/styles/BDTOPO/classique.json",
    mapboxStyleSource: "bdtopo",
    source: new ol.source.VectorTile({
        format: new ol.format.MVT(),
        url: "https://wxs.ign.fr/topographie/geoportail/tms/1.0.0/BDTOPO/{z}/{x}/{y}.pbf"
    }),
    declutter: true,
    attributions: '<a href="https://geoservices.ign.fr/blog/2020/10/26/Grand_angle_diffusion_BDTOPO.html">BDTOPO</a>',
});


export var osmLyr3 = new ol.layer.Tile({
    source: new ol.source.OSM(),
    title : "OSM",
    description: "Couche OpenStreet Map",
    visible : false
});

