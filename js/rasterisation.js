/**
 * Script principal de l'application de rasterisation d'un flux vecteur.
 * 
 * Permet de charger un style à appliquer au flux vecteur choisi, puis de choisir l'emprise de 
 * la carte à exporter au format png ou pdf, au niveau de zoom et aux dimensions voulus. Comprend
 * aussi diverses fonctionnalités.
 * 
 * Mai 2023
 * 
 * Modules externes utilisés :
 * - openlayers v7.3.0 https://openlayers.org/ (BSD 2-Clause License)
 * - ol-mapbox-style v9.7.0 https://github.com/openlayers/ol-mapbox-style (BSD 2-Clause License)
 * - html2canvas https://github.com/niklasvh/html2canvas (MIT License)
 * - jspdf https://github.com/parallax/jsPDF (MIT License)
 * 
 * @file   Script principal de l'application de rasterisation d'un flux vecteur.
 * @author Edmond Saint-Denis, Emilie Vautier, Solène Thévenet, Claire Guerrini

 */
/** */


/*--------------------- Déclaration des variables du DOM -----------------------*/

var flux = document.getElementById("flux");
var fileInput = document.getElementById('fichier_style');
var form_adresse = document.getElementById('form_adresse');
var zoom_map1 = document.getElementById("zoom-map1");
var zoom_map2 = document.getElementById("zoom-map2");
var zoom_map3 = document.getElementById("zoom-map3");
var pourcentageChargement = document.getElementById('pourcentage_chargement');
var largeur = document.getElementById('largeur');
var hauteur = document.getElementById('hauteur');
var largeur_pixel = document.getElementById('largeur_pixel');
var hauteur_pixel = document.getElementById('hauteur_pixel');
var resolutionPapier = document.getElementById('resolution');
var reechantillonnage = document.getElementById('rechantillonnage')
var formPriorite = document.getElementById('formPriorite');
var pTaille = document.getElementById('pTaille');
var pResolution =document.getElementById('pResolution');
var zoomC1 = document.getElementById('zoomC1');
var zoomC2 = document.getElementById('zoomC2');
var zoomC3 = document.getElementById('zoomC3');
var exportButton = document.getElementById('export');
var recherche = document.getElementById('recherche');
var propositionsAdresse = document.getElementById('propositions_adresse');
var zoomMap = document.getElementById('zoomMap');


/*---------------------------- Variables diverses -----------------------------*/

var layerPolygoneLoupe;

var sourceCartePrincipale;
var layersLoupeVisible;

var keyEvent;

var centre = [261204.4, 6250258.2];

var compteurTuilesDemandees = 0;
var compteurTuilesChargees = 0;

var reechantillonnageExact = 1;

/*-------------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------- CREATION DES CARTES -----------------------------------------------*/

/*------------ Création des listes de couches pour chaque carte --------------*/

import * as couches from './layers.js';

/** Couches de la carte principale */
var layers = [];

var layersLoupe=[];
var layers1 = [];
var layers2 = [];
var layers3 = [];

layers.push(couches.planignLayer);
layers.push(couches.bdtopo);
layers.push(couches.osmLyr);

layersLoupe.push(couches.planignLayerloupe);
layersLoupe.push(couches.bdtopoloupe);
layersLoupe.push(couches.osmLyrloupe);

layers1.push(couches.planignLayer1);
layers1.push(couches.bdtopo1);
layers1.push(couches.osmLyr1);

layers2.push(couches.planignLayer2);
layers2.push(couches.bdtopo2);
layers2.push(couches.osmLyr2);

layers3.push(couches.planignLayer3);
layers3.push(couches.bdtopo3);
layers3.push(couches.osmLyr3);

var listAllLayers = [layers,layersLoupe,layers1,layers2,layers3];

/**
 * Selectionne la source de la couche visible d'une carte
 * Sert pour le compteur de chargement
 * 
 * @param {Array<ol.Layer>} lLayers Liste de couches (associée à une carte)
 */
function selectSource(lLayers){
    for(const el of lLayers){
        if (el.get('visible')){
            return (el.get('source'));
        }
    }
}
sourceCartePrincipale = selectSource(layers);

/**
 * Selectionne la couche visible d'une carte
 * 
 * @param {Layer[]} lLayers Liste de couches (associée à une carte)
 */
function selectLayer(lLayers){
    for(const el of lLayers){
        if (el.get('visible')){
            return (el);
        }
    }
}
layersLoupeVisible = selectLayer(layersLoupe);


/*--------------------- Définition de la carte principale ---------------------*/ 
var map = new ol.Map({
    layers: layers,
    controls: ol.control.defaults.defaults({
        zoom: false,
        attribution: true,
        rotate: true
    }),
    target: 'map',
    view: new ol.View ({
        zoom: 10,
        center: centre
    }),
    interactions: new ol.interaction.defaults.defaults()
})


//Affichage du niveau de zoom de la carte principale
zoomMap.innerHTML = "Zoom : "+map.getView().getZoom().toFixed(1);
map.getView().on('change:resolution', function(){
    zoomMap.innerHTML = "Zoom : "+map.getView().getZoom().toFixed(1);
});

/*---------------------- Définition de la carte loupe -----------------------*/
var mapLoupe = new ol.Map({
    layers: layersLoupe,
    controls: ol.control.defaults.defaults({
        zoom: false,
        attribution: false,
        rotate: false
    }),
    target: 'maploupe',
    view: new ol.View({
        zoom: 12,
        center: centre
    }),
    interactions : new ol.interaction.defaults.defaults().extend([
        new ol.interaction.MouseWheelZoom({
            maxDelta : 0 //désactive le zoom molette pour pouvoir définir un pas différent par ailleurs
        })
    ])
});

/*------------------- Définition des cartes secondaires --------------------*/

// Initialise la première carte (zoom)
var map1 = new ol.Map({
    layers: layers1,
    controls: ol.control.defaults.defaults({
        zoom: false,
        attribution: false,
        rotate: true
    }),
    target: 'map1',
    view: new ol.View({
        zoom: parseInt(document.getElementById('zoom-map1').value),
        center: centre
    }),
    interactions: [] // Désactive toutes les interactions de la carte
});

// Crée la deuxième carte avec un niveau de zoom différent
var map2 = new ol.Map({
    layers: layers2,
    controls: ol.control.defaults.defaults({
        zoom: false,
        attribution: false,
        rotate: true
    }),
    target: 'map2',
    view: new ol.View({
        zoom:  parseInt(document.getElementById('zoom-map2').value),
        center: centre
        }),
    interactions: [] // Désactive toutes les interactions de la carte
});  

// Crée la troisième carte avec un niveau de zoom différent
var map3 = new ol.Map({
    layers: layers3,
    controls: ol.control.defaults.defaults({
        zoom: false,
        attribution: false,
        rotate: true
    }),
    target: 'map3',
    view: new ol.View({
        zoom: parseInt(document.getElementById('zoom-map3').value),
        center: centre
        }),
    interactions: [] // Désactive toutes les interactions de la carte
});


/*------------------ Calcul et affichage du chargement de la carte ---------------------*/

/**
 * Calcule et affiche l'avancement du chargement de la carte
 */
function afficherChargement(){
    let p = Math.round(compteurTuilesChargees/compteurTuilesDemandees*100);
    pourcentageChargement.innerHTML = "Les tuiles cartographiques demandées sont chargées à "+p+" %";
}

/**
 * Compte le nombre de tuiles dont le chargement a débuté et le nombre de tuiles
 * qui ont fini d'être chargées et appelle l'affichage du chargement
 */
function compterTuiles(){
    sourceCartePrincipale.on('tileloadstart',function(){
        compteurTuilesDemandees++;
        afficherChargement();
    })
    sourceCartePrincipale.on(['tileloadend', 'tileloaderror'],function(){
        compteurTuilesChargees++;
        afficherChargement();
    });
}

compterTuiles();


/*--------------------------- Echelle graphique ------------------------------*/ 

var scaleLine = new ol.control.ScaleLine({
    units: 'metric',
    bar: true,
    steps: 4,
    // text: true,
    minWidth: 60,
});
map.addControl(scaleLine);



/*-------------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------- GESTION DE LA LOUPE -----------------------------------------------*/

/*--- Génération d'une loupe permettant de visualiser un niveau de détail différent de celui de la carte de fond ---*/

var loupe = false;
var radius = 125;

document.addEventListener('keydown', function (evt) {
    // Change la taille de la loupe avec les flèches du clavier
    
    if (loupe){
        if (evt.key == "ArrowUp") {
            radius = Math.min(radius + 5, 200);
            mapLoupe.render();
            evt.preventDefault();

        } else if (evt.key == "ArrowDown") {
            radius = Math.max(radius - 5, 25);
            mapLoupe.render();
            evt.preventDefault();
        }
    }
});


var mousePositionPixel = null;

mapLoupe.getTargetElement().addEventListener('mousemove', function (event) {
    if (loupe){
        // Centrage de mapLoupe de telle manière à ce que ce qui soit au centre de la loupe corresponde aux
        // même coordonnées que la map principale
        mousePositionPixel = mapLoupe.getEventPixel(event);
        let mousePositionCoord_map = map.getCoordinateFromPixel(mousePositionPixel);
        let largeur_container = mapLoupe.getTargetElement().clientWidth;
        let hauteur_container = mapLoupe.getTargetElement().clientHeight;
        let r = mapLoupe.getView().getResolution()
        let Xc_mapLoupe = r * (largeur_container/2-mousePositionPixel[0])+mousePositionCoord_map[0];
        let Yc_mapLoupe = -r * (hauteur_container/2-mousePositionPixel[1])+mousePositionCoord_map[1];
        mapLoupe.getView().setCenter([Xc_mapLoupe,Yc_mapLoupe]);
        mapLoupe.render();
    }
});


mapLoupe.getTargetElement().addEventListener('mouseout', function () {
    if (loupe){
        // N'affiche plus la loupe quand le pointeur est en dehors de la map
        mousePositionPixel = null;
        if (zone.extent_ ==null){
            map1.getView().setCenter(map.getView().getCenter());
            map2.getView().setCenter(map.getView().getCenter());
            map3.getView().setCenter(map.getView().getCenter());
            mapLoupe.render();
        }else{
            const centre_zone = [(zone.extent_[2]+zone.extent_[0])/2,(zone.extent_[3]+zone.extent_[1])/2];
            map1.getView().setCenter(centre_zone);
            map2.getView().setCenter(centre_zone);
            map3.getView().setCenter(centre_zone);
        }
    }
});


/**
 * Découpe la carte de la loupe pour n'avoir que l'intérieur de la loupe
 */
function fLoupe(){
    // Découpage de mapLoupe pour n'afficher que ce qui est dans le cercle de la loupe
    layersLoupeVisible.on('prerender', function (event) {
        if (loupe){
            const ctx = event.context;
            ctx.save();
            ctx.beginPath();
            if (mousePositionPixel) {
            // Ne montre qu'un cercle autour de la souris
            const pixel = ol.render.getRenderPixel(event, mousePositionPixel);
            const offset = ol.render.getRenderPixel(event, [
                mousePositionPixel[0] + radius,
                mousePositionPixel[1],
            ]);
            const canvasRadius = Math.sqrt(
                Math.pow(offset[0] - pixel[0], 2) + Math.pow(offset[1] - pixel[1], 2)
            );
            ctx.arc(pixel[0], pixel[1], canvasRadius, 0, 2 * Math.PI);
            ctx.lineWidth = (5 * canvasRadius) / radius;
            ctx.strokeStyle = 'rgba(0,0,0,0.5)';
            ctx.stroke();
            }
            ctx.clip();
        }
    });

    // Après rendu, restauration du canvas context
    layersLoupeVisible.on('postrender', function (event) {
        if (loupe){
        const ctx = event.context;
        ctx.restore();
        }
    });
}

fLoupe();


/**
 * Découpe le layer qui porte la copie du polygone de selection d'emprise pour n'afficher 
 * que ce qui est dans le cercle de la loupe
 */
function fLoupePolygone(){
    
    layerPolygoneLoupe.on('prerender', function (event) {
        if (loupe){
            const ctx = event.context;
            ctx.save();
            ctx.beginPath();
            if (mousePositionPixel) {
            // only show a circle around the mouse
            const pixel = ol.render.getRenderPixel(event, mousePositionPixel);
            const offset = ol.render.getRenderPixel(event, [
                mousePositionPixel[0] + radius,
                mousePositionPixel[1],
            ]);
            const canvasRadius = Math.sqrt(
                Math.pow(offset[0] - pixel[0], 2) + Math.pow(offset[1] - pixel[1], 2)
            );
            ctx.arc(pixel[0], pixel[1], canvasRadius, 0, 2 * Math.PI);
            ctx.lineWidth = (5 * canvasRadius) / radius;
            ctx.strokeStyle = 'rgba(0,0,0,0.5)';
            ctx.stroke();
            }
            ctx.clip();
    }
    });

    layerPolygoneLoupe.on('postrender', function (event) {
        if (loupe){
        const ctx = event.context;
        ctx.restore();
        }
    });
}

/*-------- Déplacement de la carte de fond quand la loupe est présente --------*/
var previousZoom;
mapLoupe.getTargetElement().addEventListener('mousedown',function(){
if (loupe){
    mapLoupe.getTargetElement().style.zIndex = "-1"; 
    previousZoom = mapLoupe.getView().getZoom();
    mapLoupe.getView().setCenter(map.getView().getCenter())
    mapLoupe.getView().setResolution(map.getView().getResolution())
}
})

map.getTargetElement().addEventListener('mouseup', function() {
    if (loupe){
    mapLoupe.getTargetElement().style.zIndex = 1; 
    mapLoupe.getView().setZoom(previousZoom);
    }
});

mapLoupe.on('pointerdrag',function(){
    if (loupe){
        map.getView().setCenter(mapLoupe.getView().getCenter());
    }
});

/*-------------Ajout et suppression de la loupe à la demande ---------------*/

// Récupération du bouton
var boutonAff = document.getElementById("afficher-loupe");
var boutonEnlev = document.getElementById("enlever-loupe");
boutonEnlev.style.display = "none"


// Ajoute un écouteur d'événement au bouton "afficher la loupe"
boutonAff.addEventListener('click', function() {
    // Affiche le bouton "Enlever la loupe" et cacher le bouton "afficher la loupe"
    boutonAff.style.display = "none";
    boutonEnlev.style.display = "inline-block";
    mapLoupe.getTargetElement().style.zIndex = 1;
    mapLoupe.getTargetElement().style.visibility = "visible";
    loupe = true;
    mapLoupe.render();
});

// Ajoute un écouteur d'événement au bouton "désactiver la loupe"
boutonEnlev.addEventListener('click', function() {
    // Affiche le bouton "Afficher la loupe" et cacher le bouton "Enlever la loupe"
    boutonEnlev.style.display = "none";
    boutonAff.style.display = "inline-block";
    mapLoupe.getTargetElement().style.zIndex = -1;
    mapLoupe.getTargetElement().style.visibility = "hidden";
    loupe = false;
});

// Ajouter un événement pour écouter le survol de la carte principale avec la loupe
mapLoupe.on("pointermove", function (evt) {
    // Obtenir les coordonnées de la souris
    var pixel = evt.pixel;

    // Obtenir les coordonnées géographiques correspondantes
    var coord = mapLoupe.getCoordinateFromPixel(pixel);

    // Définir le centre des cartes map1, map2 et map3 sur les coordonnées de la souris
    centrerMiniCartes(coord);
});



/*-------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------- GESTION DES MINI-CARTES ---------------------------------------------*/

/**
 * Centre les minicartes sur les coordonnées en paramètre
 * @param {coordinates} coord Coordonnées sur lesquelles centrer les minicartes 
 * */
function centrerMiniCartes(coord){
    map1.getView().setCenter(coord);
    map2.getView().setCenter(coord);
    map3.getView().setCenter(coord);
}

map.on('pointerdrag',function(){
    centrerMiniCartes(map.getView().getCenter())
});


/*------------- Choix du du zoom sur les cartes d'aperçus -------------*/
zoom_map1.addEventListener('input',function(){
    map1.getView().setZoom(zoom_map1.value);
});

zoom_map2.addEventListener('input',function(){
    map2.getView().setZoom(zoom_map2.value);
});

zoom_map3.addEventListener('input',function(){
    map3.getView().setZoom(zoom_map3.value);
});

map1.getView().on('change:resolution', function(){
    if (document.activeElement != zoom_map1)
    zoom_map1.value = (map1.getView().getZoom()).toFixed(1);
})

map2.getView().on('change:resolution', function(){
    if (document.activeElement != zoom_map2)
    zoom_map2.value = (map2.getView().getZoom()).toFixed(1);
})

map3.getView().on('change:resolution', function(){
    if (document.activeElement != zoom_map3)
    zoom_map3.value = (map3.getView().getZoom()).toFixed(1);
})

mapLoupe.on('wheel', function(evt) {
    var currentZoom = mapLoupe.getView().getZoom();
    var delta = evt.originalEvent.deltaY;
    var zoom = currentZoom + (delta > 0 ? -0.1 : 0.1);
    mapLoupe.getView().setZoom(zoom);
});

/*--------- Selection de la mini carte active pour la rasterisation --------*/
var carteActive = map2
zoomC1.addEventListener('click',function(){
    carteActive = map1;
    synchroniserZoom(carteActive)
})
zoomC2.addEventListener('click',function(){
    carteActive = map2;
    synchroniserZoom(carteActive)
})
zoomC3.addEventListener('click',function(){
    carteActive = map3;centrerMiniCartes
    synchroniserZoom(carteActive)
})


/**
 * Synchronise le zoom loupe et le zoom de la carte active
 * Synchronise la taille papier avec le zoom sélectionné
 * 
 * @param {ol.Map} carte Mini carte active
 */
function synchroniserZoom(carte){
    mettreAJourPapier();
    mettreAJourPapierZoom(carte);
    ol.Observable.unByKey(keyEvent);
    keyEvent = mapLoupe.getView().on('change:resolution', function() {
        carte.getView().setZoom(mapLoupe.getView().getZoom())
    })
    mapLoupe.getView().setZoom(carte.getView().getZoom())
}
synchroniserZoom(carteActive);


/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------- GESTION DE L'APPLICATION DE STYLES ----------------------------------------*/


/*------------------ Application des styles mapbox ---------------------*/

/**
 * Applique à la couche passée en paramètre son style par défaut dont l'url est inscrit dans 
 * la définition de la couche, avec la fonction applyStyle du module ol-mapbox-style
 * 
 * @param {ol.Layer} layer Couche à laquelle on applique le style par défaut
 */
function appliquerStyle(layer) {
    
    if (layer.get('mapboxStyleUrl')) {
        fetch(layer.get('mapboxStyleUrl')).then(
            function (response) {
                if (response.ok) {
                    response.json().then(
                        function (style) {
                            olms.applyStyle(layer, style, layer.get('mapboxStyleSource'));
                        }
                    ).catch(function (error) {
                        console.error(error);
                    });
                }
            }
        );
    }

}

appliquerStyle(couches.planignLayer)
appliquerStyle(couches.planignLayer1)
appliquerStyle(couches.planignLayer2)
appliquerStyle(couches.planignLayer3)
appliquerStyle(couches.planignLayerloupe)

appliquerStyle(couches.bdtopo)
appliquerStyle(couches.bdtopo1)
appliquerStyle(couches.bdtopo2)
appliquerStyle(couches.bdtopo3)
appliquerStyle(couches.bdtopoloupe)


/*-------------------- Chargement d'un fichier de style --------------------*/

// Ajout d'un écouteur d'événement pour détecter lorsque l'utilisateur sélectionne un fichier
fileInput.addEventListener('change', function(e) {
    // Récupération du fichier sélectionné
    var file = e.target.files[0];
    
    // Création d'un objet FileReader pour lire le contenu du fichier
    var reader = new FileReader();
    
    // Ajout d'un écouteur d'événement pour détecter lorsque la lecture du fichier est terminée
    reader.addEventListener('load', function(e) {
        // Récupération du contenu du fichier et parsing du JSON
        var styleJSON = JSON.parse(e.target.result);
        listAllLayers.forEach(function(listLayers){
            for (const source in styleJSON.sources) {
                creation(styleJSON, source,listLayers);
                
            }
        });
        
    });
    
    // Lecture du contenu du fichier en tant que texte
    reader.readAsText(file);
    choisirFlux(layers.length)
    setTimeout(function(){
        layersLoupe[layersLoupe.length-1].declutter_ = false //enleve le declutter pour la couche loupe seulement, sinon les annotations dépassent du cercle
        map.addLayer(layers[layers.length-1]);
        mapLoupe.addLayer(layersLoupe[layersLoupe.length-1]);
        map1.addLayer(layers1[layers1.length-1]);
        map2.addLayer(layers2[layers2.length-1]);
        map3.addLayer(layers3[layers3.length-1]);


        layersLoupeVisible=layersLoupe[layersLoupe.length-1];
        sourceCartePrincipale = layers[layers.length-1].get('source');
        compterTuiles();
        fLoupe();
        dessinerPolygoneLoupe(zone.getExtent());


        // Création d'une nouvelle option pour la nouvelle couche
        var option = document.createElement("option");
        option.value = layers.length-1;
        option.text = file.name;
        option.selected = true
        // Ajout de l'option au formulaire déroulant
        flux.appendChild(option);

    }, 500);


    });


    /**
     * Crée une couche à partir d'un fichier JSON de style et d'une source et 
     * l'ajoute à la liste de couches en paramètre
     * 
     * @param {Object} style Objet json de style
     * @param {String} source Une des sources du style
     * @param {Array<ol.Layer>} listLayers Liste de couches à laquelle on ajoute la couche créée
     */
    var creation = function (style, source,listLayers) {
        var layer = new ol.layer.VectorTile({
               name: source,
               source: new ol.source.VectorTile({
                      format: new ol.format.MVT(),
                      url: style.sources[source].tiles[0]
               }),
               minResolution : 0,
               maxResolution : 200000,
               declutter: false
        });
        olms.applyStyle(layer, style, source).then(function () {
            listLayers.push(layer);
        });    
    }
    
/*-------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------ GESTION DES FLUX -------------------------------------------------*/

/**********************************Sélection d'une source de flux cartographique***************************/

// Génération de la liste déroulante des couches
var num_layer = 0;
layers.forEach(function(layer){
    var option = document.createElement("option");
    // Création d'une nouvelle option pour chaque couche
    option.value = num_layer;
    option.text = layer.values_.description;
    if (layer.get('visible')){
        option.selected = true
    }
    // Ajout de l'option au formulaire déroulant
    flux.appendChild(option);
    num_layer++;
});

// Détection du choix d'une autre source dans le menu déroulant
flux.addEventListener('change', function() {choisirFlux(flux.value)});

/**
 * Quand un flux est choisi, l'affiche et rend les autres invisibles
 * 
 * @param {int} choixFlux Indice du flux choisi
 */
function choisirFlux(choixFlux){
    for(let i=0;i<layers.length;i++){
        if (i!=choixFlux){
            layers[i].setVisible(false);
            layers1[i].setVisible(false);
            layers2[i].setVisible(false);
            layers3[i].setVisible(false);
            layersLoupe[i].setVisible(false);
            
        }else{
            layers[i].setVisible(true);
            layers1[i].setVisible(true);
            layers2[i].setVisible(true);
            layers3[i].setVisible(true);
            layersLoupe[i].setVisible(true);
            layersLoupeVisible=layersLoupe[i];
            sourceCartePrincipale = layers[i].get('source');
            compterTuiles();
            fLoupe();
        }
    }

}



/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------- GESTION DE LA RECHERCHE D'ADRESSE -----------------------------------------*/


form_adresse.addEventListener('submit',searchAddress);
recherche.addEventListener('input',autocomplete)

/**
 * Envoie une requete a l'API de la BD Adresse à partir du contenu de la barre de recherche
 * d'adresse et recupere le premier résultat d'adresse puis centre la carte dessus
 * 
 * @param {*} e Evenement
 */
function searchAddress(e){
    
    e.preventDefault();
    clearPropositions()


    let adresse  = recherche.value;

    //Requete a l'api BD Adresse 
    if (adresse.length > 2) {
        
        fetch('http://api-adresse.data.gouv.fr/search/?q='+adresse+'&limit=1')
        .then(result => result.json())
        .then(result => {

            //On centre la carte sur le resultat en adaptant la vue
            centerOnAddress(result.features[0])
        })
    }
}


/**
 * Envoie une requete a l'API de la BD Adresse à partir du contenu de la barre de recherche
 * d'adresse et recupere les premiers résultats d'adresses, puis les affiche dans un menu
 * déroulant qui permet à l'utilisateur de choisir l'une des propositions et centre la carte
 * dessus
 * 
 * @param {*} e Evenement
 */
function autocomplete () {

    clearPropositions()

    let adresse  = recherche.value;

    //Requête à l'api BD Adresse 
    if (adresse.length > 2) {

        fetch('http://api-adresse.data.gouv.fr/search/?q='+adresse+'&limit=10')
        .then(result => result.json())
        .then(result => {

            //De chaque feature resultat, correspondant a une adresse, on fait une proposition
            //d'autocompletion que l'on peut selectionner dans le menu deroulant
            result.features.forEach( function (feature) {

                let proposition = document.createElement('button');
                proposition.classList.add("proposition_button")
                proposition.innerText = feature.properties.label;

                //La proposition peut être choisie quand on clique dessus
                proposition.addEventListener('click',function (e) {chooseProposition(e,feature)})

                //Ajout de la proposition au menu deroulant
                propositionsAdresse.appendChild(proposition);
                })
        }) 
    }
};

/**
 * Quand une proposition d'aucomplétion d'adresse est selectionnée, écrit l'adresse
 * dans la barre de recherche et centre directement la carte sur les coordonnées du feature associé
 * 
 * @param {*} e Evénement
 * @param {Feature} feature Feature correspondant à une adresse dans le JSON résultat d'une requête a l'API BD Adresse
 */
function chooseProposition (e,feature) {
    recherche.value = e.target.innerText
    centerOnAddress(feature)
    clearPropositions()
}
    
/**
 * Vide le menu deroulant des propositions d'autocomplétion
 */
function clearPropositions () {
    while (propositionsAdresse.firstChild) {
        propositionsAdresse.removeChild(propositionsAdresse.firstChild);
    }
}


/**
 * Centre et zoom la carte sur l'adresse donnée
 * @param {Feature} feature Feature correspondant a une adresse dans le JSON resultat d'une requete a l'API BD Adresse
 */
function centerOnAddress(feature) {

    let coord = feature.geometry.coordinates;
    map.getView().setCenter(ol.proj.fromLonLat(coord));
    centrerMiniCartes(ol.proj.fromLonLat(coord));
    map.getView().setZoom(11);
}






/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------- GESTION DE SELECTION DE LA ZONE A EXPORTER ---------------------------------------*/


/*---------- Ajout de la possibilité de créer une zone de sélection -----------*/

var zone = new ol.interaction.Extent({
condition: ol.events.condition.shiftKeyOnly,
boxStyle : [
    new ol.style.Style({
        fill: new ol.style.Fill({
            color: [0, 0, 255, 0.3],
        }),
    }),
    ]
});
map.addInteraction(zone);

// Quand la zone change on met à jour les dimension de l'impression et la zone affichée sur la carte loupe
zone.on('extentchanged', function(){
    afficherTaillePapier(zone);
    dessinerPolygoneLoupe(zone.getExtent());
});



/**
 * Dessine les contours de la zone de sélection sur la carte loupe
 * 
 * @param {Extent} etendue Etendue terrain à dessiner sur la carte loupe
 */
function dessinerPolygoneLoupe(etendue){
    mapLoupe.removeLayer(layerPolygoneLoupe);
    if (etendue != null){
        const polygone = new ol.geom.Polygon.fromExtent(etendue);
        let feature = new ol.Feature({
            geometry : polygone,
        })
        let source = new ol.source.Vector({
            features : [feature]
        });

        layerPolygoneLoupe = new ol.layer.Vector({
            source : source,
            style :    new ol.style.Style({
                fill: new ol.style.Fill({
                    color: [0, 0, 255, 0.3],
                }),
            }),
        })
        mapLoupe.addLayer(layerPolygoneLoupe);
        fLoupePolygone();
    }
}

/**
 * Affiche la taille de l'image au format papier dans les champs correspondants
 * à partir de l'étendue sélectionnée sur la carte
 * 
 * @param {Extent} zone Etendue terrain de la zone de sélection
*/
function afficherTaillePapier(zone){
    if (zone.getExtent() != null){
        const resolutionCible = carteActive.getView().getResolution();
        const largeurZone = zone.extent_[2]-zone.extent_[0];
        const hauteurZone = zone.extent_[3]-zone.extent_[1];
        const largeurPapier = largeurZone*2.54/(resolutionCible*resolutionPapier.value);
        const hauteurPapier = hauteurZone*2.54/(resolutionCible*resolutionPapier.value);
        const largeurPixel = largeurZone / resolutionCible/reechantillonnageExact;
        const hauteurPixel = hauteurZone / resolutionCible/reechantillonnageExact;

        if (document.activeElement != largeur){
            largeur.value = (largeurPapier).toFixed(2);
        }
        if (document.activeElement != largeur_pixel){
            largeur_pixel.value = largeurPixel.toFixed(0);
        }
        if (document.activeElement != hauteur){
            hauteur.value = (hauteurPapier).toFixed(2);
        }
        if (document.activeElement != hauteur_pixel){
            hauteur_pixel.value = hauteurPixel.toFixed(0);
        }

        
    };
}

/**
 * Affiche la taille de l'image au format papier dans le champ correspondant
 * à partir de l'étendue sélectionnée sur la carte
 * 
 * @param {Extent} zone Etendue terrain de la zone de sélection
 */
function afficherResolution(zone){
    if (zone.getExtent() != null){
        const resolutionCible = carteActive.getView().getResolution();
        const largeurProj = zone.extent_[2]-zone.extent_[0];
        const largeurPixel = largeurProj / resolutionCible;
        const resolution = largeurPixel*2.54/largeur.value;
        reechantillonnageExact = largeurPixel / largeur_pixel.value;
        resolutionPapier.value = resolution.toFixed(1);
        reechantillonnage.value = reechantillonnageExact.toFixed(2);
    }
}

/**
 * Met à jour la valeur des champs de taille ou de résolution selon la priorité,
 * à chaque fois que l'on change le zoom de la loupe ou de la mini-carte sélectionnée
 * 
 * @param {ol.Map} carte Carte dont on récupère le zoom pour mettre à jour les champs
 */
function mettreAJourPapierZoom(carte){
    carte.getView().on('change:resolution', function() {
        if (priorite =="resolution"){
            afficherTaillePapier(zone);
        }
        if (priorite =="taille"){
            afficherResolution(zone);
        }
    });
}
mettreAJourPapierZoom(carteActive);


/**
 * Met à jour la valeur des champs de taille ou de résolution, selon la priorité
 */
function mettreAJourPapier(){
    if (priorite =="resolution"){
        afficherTaillePapier(zone);
    }
    if (priorite =="taille"){
        afficherResolution(zone);
    }
}


/* ------------- Adaptation de la zone de sélection à la taille papier -------------*/

largeur.addEventListener('input',zoneAdaptCm);
hauteur.addEventListener('input',zoneAdaptCm);

largeur_pixel.addEventListener('input',zoneAdaptPx)
hauteur_pixel.addEventListener('input',zoneAdaptPx)

resolutionPapier.addEventListener('input',zoneAdaptCm);
reechantillonnage.addEventListener('input',zoneAdaptPx);

/**
 * Adapte l'étendue de la zone de sélection sur la carte aux dimensions choisies
 * en cm dans les champs correspondants
 */
function zoneAdaptCm(){
    if (largeur.value!="" && hauteur.value!=""){
        var l = Number(largeur.value)*10;
        var h = Number(hauteur.value)*10;
        const resolutionCible = carteActive.getView().getResolution();
        var centreZone;

        if (zone.getExtent()!=null){
            centreZone = [(zone.getExtent()[0]+zone.getExtent()[2])/2,(zone.getExtent()[1]+zone.getExtent()[3])/2]
        }else{
            centreZone = map.getView().getCenter();
        }
        const largeurProj = l * resolutionCible*resolutionPapier.value/25.4;
        const hauteurProj = h * resolutionCible*resolutionPapier.value/25.4;
        const etendue = [centreZone[0]-largeurProj/2,centreZone[1]-hauteurProj/2,
                        centreZone[0]+largeurProj/2,centreZone[1]+hauteurProj/2]
        zone.setExtent(etendue);
    }
}

/**
 * Adapte l'étendue de la zone de sélection sur la carte aux dimensions choisies
 * en pixel dans les champs correspondants
 */
function zoneAdaptPx(){
    if (largeur_pixel.value!="" && hauteur_pixel.value!=""){
        reechantillonnageExact = reechantillonnage.value
        const resolutionCible = carteActive.getView().getResolution();
        var centreZone;
        if (zone.getExtent()!=null){
            centreZone = [(zone.getExtent()[0]+zone.getExtent()[2])/2,(zone.getExtent()[1]+zone.getExtent()[3])/2]
        }else{
            centreZone = map.getView().getCenter();
        }
        const largeurProj = largeur_pixel.value * reechantillonnageExact*resolutionCible;
        const hauteurProj = hauteur_pixel.value * reechantillonnageExact*resolutionCible;
        const etendue = [centreZone[0]-largeurProj/2,centreZone[1]-hauteurProj/2,
                        centreZone[0]+largeurProj/2,centreZone[1]+hauteurProj/2]
        zone.setExtent(etendue);
    }
}


/* ---- Déplacement de la zone de sélection à l'aide des touches directionnelles du clavier ----*/

window.addEventListener("keydown", function (event) {
if (!loupe){
    if (event.defaultPrevented) {
        return;
    }
    var deltaH = 0;
    var deltaV = 0;
    const resolutionCarte = map.getView().getResolution();
    switch (event.key) {
        case "ArrowDown":
        deltaV = -resolutionCarte
        break;
        case "ArrowUp":
        deltaV = resolutionCarte
        break;
        case "ArrowLeft":
        deltaH = -resolutionCarte
        break;
        case "ArrowRight":
        deltaH = resolutionCarte
        break;
        default:
        return; // Quitter lorsque cela ne gère pas l'événement touche.
    }
    event.preventDefault();// Annuler l'action par défaut pour éviter qu'elle ne soit traitée deux fois.
    if (zone.getExtent()!=null){
        var etendueZone = zone.getExtent();
        etendueZone[0] += deltaH;
        etendueZone[2] += deltaH;
        etendueZone[1] += deltaV;
        etendueZone[3] += deltaV;
        zone.setExtent(etendueZone);
    }
}
}, true);


// Priorisation de la taille ou de la résolution en cas d'impression en PDF
var priorite = 'resolution'
formPriorite.addEventListener('change',function(){
    if (pTaille.checked){
        priorite = 'taille'
    }
    if (pResolution.checked){
        priorite = 'resolution'
    }
})



/*-------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------ GESTION DE LA RASTERISATION --------------------------------------------*/


// export options for html2canvase.
// See: https://html2canvas.hertzen.com/configuration
const exportOptions = {
    useCORS: true,
    ignoreElements: function (element) {
        const className = element.className || '';
        return (
        className.includes('ol-control') &&
        !className.includes('ol-scale') &&
        (!className.includes('ol-attribution') ||
            !className.includes('ol-uncollapsible'))
        );
    },
};




exportButton.addEventListener(
    'click',
    /**
     * Lance le processus de rasterisation pour exportation de la zone sélectionnée
     * sous forme de raster, au format png ou pdf.
     */
    function () {

        let PDF = document.getElementById('PDF').checked;
        let image = document.getElementById('Image').checked;
        let echelle = document.getElementById('echelle').checked;

        if (zone.extent_ != null && (PDF || image)){
            
            if (!echelle){
                map.removeControl(scaleLine);
            }

            pourcentageChargement.style.visibility = "visible";
            exportButton.disabled = true;
            document.body.style.cursor = 'progress';

            //Conversion du centre de la zone en WGS84 pour le nommage du fichier
            const centre_zone = [(zone.extent_[2]+zone.extent_[0])/2,(zone.extent_[3]+zone.extent_[1])/2];
            const centre_zoneWGS84 = ol.proj.transform(centre_zone, 'EPSG:3857','EPSG:4326');

            //Récupération de la largeur et de la hauteur de la zone de sélection avant de l'enlever
            const largeurProj = zone.extent_[2]-zone.extent_[0];
            const hauteurProj = zone.extent_[3]-zone.extent_[1];
            map.removeInteraction(zone);

            //Sauvegarde du centre et de la résolution de la carte principale 
            const resolutionOrigine = map.getView().getResolution();
            const centreOrigine = map.getView().getCenter();

            //Transformation de la taille et de la résolution de la carte pour qu'elle corresponde
            //au contenu de la zone sélectionnée à la résolution voulue
            const resolutionCible = carteActive.getView().getResolution();
            const nivZoom = carteActive.getView().getZoom();
            const FileName = 'carte_centre_lon'+centre_zoneWGS84[0].toFixed(4)+'-lat'+centre_zoneWGS84[1].toFixed(4)+'_zoom'+nivZoom.toFixed(2);
            const width = Math.round(largeurProj / resolutionCible);
            const height = Math.round(hauteurProj / resolutionCible);
            const taillePapier = [width*25.4/resolutionPapier.value,height*25.4/resolutionPapier.value];
            const scale = reechantillonnageExact;
            map.getTargetElement().style.width = width + 'px';
            map.getTargetElement().style.height = height + 'px';
            map.updateSize();
            map.getView().setResolution(resolutionCible);
            map.getView().setCenter(centre_zone)
            
            //Lancement du render de la carte
            map.render()

            compteurTuilesChargees = 0;
            compteurTuilesDemandees = 0; 
            
            const formatPapier = (taillePapier[0]>=taillePapier[1])? "landscape" : "portrait";

            //Une fois le rendu terminé, le processus d'exportation se lance
            map.once('rendercomplete', function () {
                pourcentageChargement.innerHTML = "Les tuiles sont chargées. Création des images";
                exportOptions.width = width;
                exportOptions.height = height;

                if (image) {
                    exportOptions.scale = 1/scale;
                    html2canvas(map.getViewport(), exportOptions).then(function (canvas) {
                    var createImage = canvas.toDataURL('image/png',1);
                    
                    // Téléchargement au format image
                    var aDownloadLink = document.createElement('a');
                    aDownloadLink.download = FileName+'.png';
                    aDownloadLink.href = createImage;
                    aDownloadLink.click();
                    });
                };

                if (PDF) {
                    exportOptions.scale = 1;
                    html2canvas(map.getViewport(), exportOptions).then(function (canvas) {
                    var createImage = canvas.toDataURL('image/png',1);
                    
                    // Téléchargement au format pdf
                    const pdf = new jspdf.jsPDF(formatPapier,"mm",taillePapier);
                    pdf.addImage(
                    createImage,
                    'PNG',
                    0,
                    0,
                    taillePapier[0],
                    taillePapier[1]
                    );
                    pdf.save(FileName+'.pdf');
                    });
                }
                pourcentageChargement.innerHTML = "Les opérations sont terminées"
                

                //Restauration de la carte principale originale
                scaleLine.setDpi();
                map.getTargetElement().style.width = "";
                map.getTargetElement().style.height = "";
                map.updateSize();
                map.getView().setResolution(resolutionOrigine);
                map.getView().setCenter(centreOrigine);
                map.addInteraction(zone);
                exportButton.disabled = false;
                document.body.style.cursor = 'auto';
                console.log("nombre de tuiles demandées : ",compteurTuilesDemandees);
                console.log("nombre de tuiles chargées : ",compteurTuilesChargees);
                map.addControl(scaleLine);
                });
            
            
        }else{
            pourcentageChargement.style.visibility = "visible"
            pourcentageChargement.innerHTML = "Veuillez fixer une emprise géographique et choisir un ou plusieurs types d'enregistrement";
        }
    }
);
