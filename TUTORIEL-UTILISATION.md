# Tutoriel d’utilisation 

Ce document décrit le tutoriel d'utilisation de l'interface web Rasterisation d'un flux vecteur.

* Pour commencer, deux possibilités :
  - soit choisir une source de flux cartographique dans le menu déroulant situé à gauche. Trois sources différentes sont proposées : Plan IGN vecteur tuilé, BDTOPO vecteur tuilé, couche OpenStreetMap.
  - soit charger un fichier de source de données avec un style associé, au format JSON (quelques exemples sont fournis dans le dossier [styles](styles)). Ce fichier va créer une nouvelle couche cartographique qui va s'appliquer sur les cartes.

* Rechercher une adresse, pour centrer la carte sur un lieu précis. L’autocomplétion permet d’afficher un certain nombre de lieux possibles à partir de quelques lettres tapées.

* Cliquer sur le bouton “afficher la loupe” pour zoomer sur une petite partie de la carte principale et visualiser différents niveaux de détails. Quand on déplace la loupe sur la carte, l’emprise des trois petites cartes situées en bas se déplace en même temps. Il est possible d’agrandir ou de rétrécir l’emprise de la loupe grâce aux flèches du haut et du bas du clavier et de changer le zoom de la loupe avec la molette de la souris. Pour faire disparaitre la loupe, cliquer sur le bouton “enlever la loupe”.

* Le niveau de détails de la carte finale va être celui d’une des trois petites cartes du bas (carte sélectionnée en cliquant sur le petit rond à côté du zoom).  Pour chacune des petites cartes il est possible de taper manuellement un zoom précis.

* Pour fixer l’emprise de la carte finale il y a deux possibilités : soit taper manuellement la largeur et la longueur de l’emprise en centimètres ou en pixels dans les cases prévues à cet effet, soit tracer le rectangle de l’emprise en faisant un SHIFT + clic sur la carte principale. Il est possible de déplacer le rectangle de l’emprise sur la carte principale grâce aux flèches du clavier, chaque appui déplace l’emprise d’un pixel.

* Indiquer la qualité d’impression voulue en dpi (points par pouces) pour une sortie PDF ou en ratio entre l’image cartographique et l’image crée pour une sortie PNG.

* Lors du changement de zoom à l’aide de la loupe ou des mini-cartes on a deux possibilités : verrouiller la taille d’impression définie, et donc modifier la qualité de la sortie, ou verrouiller la qualité d’impression et donc modifier les dimensions de l’image. Il est déconseillé d’utiliser un rééchantillonnage trop élevé (donc de fusionner des pixels cartographiques pour créer le raster de sortie).

* Si l’on veut afficher la barre d’échelle sur la carte, cocher la case correspondante.

* Cocher PDF et/ou Image puis appuyer sur le bouton “Exporter” pour enregistrer la carte rastérisée au format choisi.
